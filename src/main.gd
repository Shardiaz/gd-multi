extends Node2D

@export var playerScene = preload("res://src/Player.tscn")
@export var popupScene = preload("res://src/Popup.tscn")

func _ready():
	multiplayer.peer_connected.connect(self.peer_connected)
	multiplayer.peer_disconnected.connect(self.peer_disconnected)
	multiplayer.server_disconnected.connect(self.server_disconneted)
	multiplayer.connected_to_server.connect(self.connected_to_server)
	multiplayer.connection_failed.connect(self.connection_failed)

func popup(label:String, message:String):
	var instance = popupScene.instantiate()
	instance.label = label
	instance.message = message
	$Popups.add_child(instance)

	
func peer_disconnected(id: int):
	popup("peer disconnected", str(id))

func peer_connected(id: int):
	popup("peer connected", str(id))
	
func server_disconneted():
	popup("server disconneted", " ")
	
func connected_to_server():
	popup("connected to server", " ")
	
func connection_failed():
	popup("connection failed", " ")
	
