extends Node2D
class_name LobbyPlayer

@onready var PlayerName = $Label as Label
@onready var AnimatedSprite = $AnimatedSprite2d as AnimatedSprite2D
@onready var Synchronizer = $MultiplayerSynchronizer as MultiplayerSynchronizer
@onready var ReadyIcon = $ReadyIcon as Sprite2D

@export var player_ready: bool:
	set(value):
		player_ready = value
		if ReadyIcon:
			ReadyIcon.visible = value

@export var player_name: String:
	set(value):
		player_name = value
		if PlayerName:
			PlayerName.text = value

@export var player_animation: String:
	set(value):
		player_animation = value
		if AnimatedSprite:
			AnimatedSprite.animation = player_animation
					

# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerName.text = player_name
	AnimatedSprite.animation = player_animation

func set_player_animation(new_animation: String):
	rpc_id(1, "request_animation", new_animation)

func set_player_name(new_player_name: String):
	rpc_id(1, "request_player_name", new_player_name)
	
func set_is_ready(is_ready: bool):
	rpc_id(1, "request_is_ready", is_ready)

@rpc(call_remote,any_peer)
func request_player_name(requested_player_name: String):
	if str(name).to_int() == multiplayer.get_remote_sender_id():
		player_name = requested_player_name

@rpc(call_remote,any_peer)
func request_animation(requested_player_animation: String):
	if str(name).to_int() == multiplayer.get_remote_sender_id():
		player_animation = requested_player_animation

@rpc(call_remote,any_peer)
func request_is_ready(is_ready: bool):
	if str(name).to_int() == multiplayer.get_remote_sender_id():
		player_ready = is_ready