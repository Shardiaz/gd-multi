extends CharacterBody2D

class_name Player

signal spawn_ability(ability)

@export var primary: PackedScene =  preload("res://src/Arrow.tscn")
@export var secondary: PackedScene =  preload("res://src/Arrow.tscn")

@onready var PlayerSchnronizer = $PlayerSchnronizer
@onready var BulletPosition = $BulletPosition
@onready var AnimatedSprite = $AnimatedSprite
@onready var Profile = $Profile
@onready var UI = $UI

#Player profile
var player_name: String = "Kingsley"
var color: Color = Color.AQUA

#Battler data
var max_hp: int = 100
@export var hp: int = 100
var speed = 60 * 300.0
var jump_velocity = -400.0
var just_joined = true
var move_direction: float = 0

#
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _ready():
	Profile.ProfileSynchronizer.set_multiplayer_authority(str(name).to_int())
	update_ui()
	get_tree().create_timer(1.5).timeout.connect(self.activate)

func activate():
	just_joined = false
	modulate = Color.WHITE

func _process(_delta):
	if not is_local_authority():
		return
		
	if Input.is_action_just_pressed('primary_ability'):
		rpc_id(1, "primary_ability", get_global_mouse_position())

	if Input.is_action_just_pressed('secondary_ability'):
		rpc_id(1, "secondary_ability", get_global_mouse_position())
	
@rpc(call_remote,any_peer)
func primary_ability(target: Vector2):
	use_ability(primary, target, multiplayer.get_remote_sender_id())

@rpc(call_remote,any_peer)
func secondary_ability(target: Vector2):
	use_ability(secondary, target, multiplayer.get_remote_sender_id())

func use_ability(packedScene: PackedScene, target: Vector2, owner_id: int):
	var instance = packedScene.instantiate() as Ability
	instance.target_position = target
	instance.owner_id = owner_id
	spawn_ability.emit(instance)
	
func _physics_process(delta):
	if move_direction:
		velocity.x = move_direction * speed * delta
	else:
		velocity.x = move_toward(velocity.x, 0, speed * delta)

	if not is_on_floor(): 
		velocity.y += gravity * delta

	if move_direction > 0:
		AnimatedSprite.flip_h =true
	elif move_direction < 0:
		AnimatedSprite.flip_h = false

	move_direction = 0
	move_and_slide()
	update_ui()

	if not is_local_authority():
		return

	# Handle Jump.
	if Input.is_action_just_pressed("jump"):
		rpc_id(1,"request_jump")

	# Get the input direction and handle the movement/deceleration.
	var direction = Input.get_action_strength("move_right") - Input.get_action_strength("move_left");
	if direction:
		rpc_id(1,"request_move", direction)

	
@rpc(call_remote,any_peer)
func request_move(direction: float) :
	move_direction = direction;


@rpc(call_remote,any_peer)
func request_jump():
	if is_on_floor():
		velocity.y = jump_velocity
	
	
func update_ui():
	UI.visible = is_local_authority()
	if is_local_authority():
		UI.update_stats(self)

func is_local_authority():
	return str(name).to_int() == multiplayer.get_unique_id()
	
@rpc(call_local)
func alter_hp(value: int):
	if just_joined:
		return
		
	hp = clamp(hp + value,0,max_hp)	

	if hp == 0:
		queue_free()
		return	
