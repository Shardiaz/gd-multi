extends Control

@onready var Hp = $Hp
@onready var HpProgress = $Hp/MarginContainer/HpProgress
@onready var PlayerName = $PlayerName

func update_stats(player: Player):
	if not visible:
		return
	
	Hp.visible = player.max_hp > player.hp
	HpProgress.max_value = player.max_hp
	HpProgress.value = player.hp
	PlayerName.text = player.player_name
#
