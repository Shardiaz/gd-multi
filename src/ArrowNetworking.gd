extends MultiplayerSynchronizer
 
var sync_position: Vector2:
		set(value):
			sync_position = value
			processed_position = false
var processed_position: bool
var sync_rotation: float;

func process_ability(ability: RigidBody2D):
	if not processed_position:
		ability.position = sync_position
		ability.rotation = sync_rotation
		processed_position = true

func sync_ability(ability: RigidBody2D):
	sync_position = ability.position
	sync_rotation = ability.rotation
