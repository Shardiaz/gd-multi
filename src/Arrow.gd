extends RigidBody2D

class_name Ability

@onready var CollisionShape = $CollisionShape
@onready var Sprite = $AnimatedSprite
@onready var Networking = $Networking

var owner_id: int
var speed: int = 1000
var fire_rate: float = 0.2
var strength = 20
var target_position: Vector2

func destroy():
	CollisionShape.set_deferred("disable_collision", true)
	set_physics_process(false)
	queue_free()

func _physics_process(_delta):
	if not multiplayer.is_server():
		Networking.process_ability(self)
	else: 
		Networking.sync_ability(self)

func execute(player):
	position = player.BulletPosition.global_position
	look_at(target_position)
	apply_impulse(Vector2(speed, 0).rotated(rotation))
		

func _on_arrow_body_entered(body):
	if not multiplayer.is_server():
		return;
	if str(body.name).to_int() == owner_id:
		max_contacts_reported += 1
		return
	if body.is_in_group("player"):
		body.rpc_id(1, "alter_hp", strength * -1)
	destroy()

func _on_visible_on_screen_notifier_2d_screen_exited():
	set_physics_process(false)
	if multiplayer.is_server():
		queue_free()
