extends CharacterBody2D

const ATTACK = 1
const DIE = 2
const JUMP = 3

@onready var sprite = $Sprite as Sprite2D
@onready var animation_tree = $AnimationTree as AnimationTree
var animation_playback
var actions = []

const SPEED = 300.0 * 60
const JUMP_VELOCITY = -800.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _ready():
	animation_playback = animation_tree.get("parameters/playback")

func _physics_process(delta):
	movement_loop(delta)

func _unhandled_input(event):
	if event.is_action_pressed("primary_ability"):
		use_ability()
	if event.is_action_pressed("secondary_ability"):
		use_ability()

func movement_loop(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta
	else:
		actions.erase(JUMP)

	# Handle Jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
		actions.append(JUMP)
		animation_playback.travel("jump")

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("move_left", "move_right")
	if direction:
		velocity.x = direction * delta * SPEED
		sprite.flip_h = direction < 0
		if not actions.size():
			animation_playback.travel("run")
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED * delta)
		if not actions.size():
			sprite.flip_h = false
			animation_playback.travel("idle")

	move_and_slide()

func use_ability():
	if actions.has(ATTACK):
		return

	actions.append(ATTACK)
	if is_on_floor():
		animation_playback.travel("attack")
	else:
		animation_playback.travel("jump-attack")

	await get_tree().create_timer(.5).timeout
	actions.erase(ATTACK)


func _on_button_pressed():
	set_physics_process(false)
	set_process_input(false)
	animation_playback.travel("die")
