extends Node2D

@onready var Address = %Address as TextEdit
@onready var Port = %Port as SpinBox
@onready var JoinUI = $JoinUI as Panel
@onready var ServerUI = $ServerUI as Panel
@onready var PlayerUI = $PlayerUI as PlayerUI
@onready var Players = $Players
@onready var LobbySpawner = $LobbySpawner as MultiplayerSpawner

signal start_game()
var ready_count: int = 0

var PopupScene = preload("res://src/Popup.tscn")
var LobbPlayerScene = preload("res://src/LobbyPlayer/LobbyPlayer.tscn")

func _ready():
	var arguments = read_cmd_line_args()
	if arguments.has("port"):
		Port.value = str(arguments["port"]).to_int()
	if arguments.has("address"):
		Address.text = arguments["address"]
	if arguments.has("server"):
		start_network(true)
	
func read_cmd_line_args():
	var arguments = {}
	for argument in OS.get_cmdline_args():
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			arguments[key_value[0].lstrip("--")] = key_value[1]
		else: 
			arguments[argument.lstrip("--")] = true

	return arguments

func start_network(is_server: bool):
	var peer = ENetMultiplayerPeer.new()
	
	if is_server:
		var error = peer.create_server(Port.value)
		if error:
			popup("Unable to start server", get_error_message(error))
			return
		show_server_ui()

		multiplayer.peer_connected.connect(self.create_player)
		multiplayer.peer_disconnected.connect(self.destroy_player)

		print("Gameserver running on %s:%s" % [Address.text ,Port.value])
	else:
		var error = peer.create_client(Address.text, Port.value)
		if error:
			popup("Unable to join server", get_error_message(error))
			return
		
		show_player_ui()
		multiplayer.server_disconnected.connect(self.server_disconneted)
	
	multiplayer.set_multiplayer_peer(peer)

func show_server_ui():
	JoinUI.visible = false;
	PlayerUI.visible = false;
	ServerUI.visible = true;

func show_player_ui():
	JoinUI.visible = false;
	PlayerUI.visible = true;
	ServerUI.visible = false;

func show_join_ui():
	JoinUI.visible = true;
	PlayerUI.visible = false;
	ServerUI.visible = false;

func create_player(id:int):
	var data = {}
	data.name = str(id)
	data.player_name = "Player %s" % (Players.get_child_count() + 1)
	data.player_animation = PlayerSprite.random_animation()
	data.x = Players.get_child_count() * 200
	LobbySpawner.spawn(data)

func destroy_player(id:int):
	var player = Players.get_node(str(id))
	if player:
		player.queue_free()

func server_disconneted():
	show_join_ui()
	
func get_error_message(code: int):
	match code:
		ERR_CANT_CREATE:
			return "Unable to create (ERR_CANT_CREATE)"
		
		ERR_ALREADY_IN_USE:
			return "Peer already in use (ERR_ALREADY_IN_USE)"

		ERR_CANT_CONNECT:
			return "Unable to connect (ERR_CANT_CONNECT)"

	
func _on_quit_pressed():
	get_tree().quit()

func _on_join_game_pressed():
	start_network(false)

func popup(label:String, message:String):
	var instance = PopupScene.instantiate()
	instance.label = label
	instance.message = message
	$Popups.add_child(instance)
	