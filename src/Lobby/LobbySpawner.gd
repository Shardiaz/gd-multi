extends MultiplayerSpawner

signal local_player(player: LobbyPlayer)

var LobbyPlayer = preload("res://src/LobbyPlayer/LobbyPlayer.tscn")

func _spawn_custom(data: Variant):
	var instance = LobbyPlayer.instantiate() as LobbyPlayer
	instance.name = data.name
	instance.player_name = data.player_name
	instance.player_animation = data.player_animation
	instance.position.x = data.x

	if data.name == str(multiplayer.get_unique_id()):
		local_player.emit(instance)
	
	return instance
