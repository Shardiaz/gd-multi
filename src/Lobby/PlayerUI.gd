extends Panel
class_name PlayerUI

@onready var PlayerName = $PlayerName as TextEdit
@onready var AnimatedSprite = $AnimatedSprite2d as AnimatedSprite2D

var player : LobbyPlayer

@export var player_name: String:
	set(value):
		PlayerName.text = value
	get:
		return PlayerName.text

@export var player_animation: String:
	set(value):
		AnimatedSprite.animation = value
	get:
		return AnimatedSprite.animation


func _on_previous_pressed():
	AnimatedSprite.next()
	player.set_player_animation(AnimatedSprite.animation)

func _on_next_pressed():
	AnimatedSprite.previous()
	player.set_player_animation(AnimatedSprite.animation)

func _on_lobby_spawner_local_player(spawned_player:LobbyPlayer):
	player = spawned_player
	player_name = spawned_player.player_name
	player_animation = spawned_player.player_animation
	AnimatedSprite.visible = true

func _on_check_button_toggled(button_pressed:bool):
	player.set_is_ready(button_pressed)

func _on_player_name_text_changed():
	player.set_player_name(PlayerName.text)

