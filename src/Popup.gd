extends PanelContainer

@export var label: String
@export var message: String

func _ready():
	get_tree().create_timer(2).timeout.connect(self.destroy)
	$MarginContainer/GridContainer/Label.text = label
	$MarginContainer/GridContainer/Message.text = message


func destroy():
	queue_free()
