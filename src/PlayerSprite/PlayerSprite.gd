extends AnimatedSprite2D
class_name PlayerSprite

const animations =[
	"jack",
	"knight",
	"ninja",
	"ninja-girl",
	"robot",
	"zombie",
	"zombie-girl"
]

static func random_animation():
	return animations[randi() % animations.size()]

func next():
	animation = animations[(animations.find(str(animation))+1) % animations.size()]
	

func previous():
	animation = animations[(animations.find(str(animation))-1+animations.size()) % animations.size()]

